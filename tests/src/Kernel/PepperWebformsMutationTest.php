<?php

namespace Drupal\Tests\pepper_webforms\Kernel;

use Drupal\Tests\pepper_graphql\Kernel\PepperKernelTestBase;
use Drupal\webform\Entity\Webform;

/**
 * Test class for the SubmitForm mutation.
 *
 * @group pepper_webforms
 */
class PepperWebformsMutationTest extends PepperKernelTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'pepper_graphql',
    'webform',
    'pepper_webforms',
    'field',
  ];

  /**
   * The webform created for the test.
   *
   * @var \Drupal\webform\Entity\Webform
   */
  private $webform;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {

    parent::setUp();

    $this->installSchema('webform', ['webform']);
    $this->installConfig('webform');
    $this->installEntitySchema('webform_submission');

    // Create the webform with every (at the moment) possible element.
    /** @var \Drupal\webform\WebformInterface $webform */
    $this->webform = Webform::create(['id' => 'contact_form', 'title' => 'Contact Form']);
    $elements = [
      'first_name' => [
        '#type' => 'textfield',
        '#title' => 'First name',
        '#required' => TRUE,
      ],
      'salutation' => [
        '#type' => 'select',
        '#title' => 'Salutation',
        '#required' => TRUE,
        '#options' => [
          'm' => 'Mr.',
          'f' => 'Mrs.',
        ],
      ],
      'email' => [
        '#type' => 'email',
        '#title' => 'E-Mail',
        '#required' => TRUE,
      ],
      'privacy_policy' => [
        '#type' => 'checkbox',
        '#title' => 'I accept the terms',
        '#required' => TRUE,
      ],
    ];
    $this->webform->setElements($elements);
    $this->webform->save();

    $this->loadModuleSchema();

  }

  /**
   * @covers \Drupal\pepper_webforms\Plugin\GraphQL\DataProducer\Webform\Mutation::resolve
   */
  public function testPepperWebformsSubmitForm() {
    // Test a correct webform submission.
    $data = [
      'id' => 'contact_form',
      'values' => '{"salutation":"m","first_name":"Roman","email":"roman.domke@unic.com","privacy_policy":true}',
    ];
    $result = $this->executeDataProducer('submit_form', [
      'data' => $data,
      'language' => 'DE',
    ]);

    $this->assertEquals(1, $result['submission']['submission_id']);

    // Test error handling while submitting a webform.
    // Webform does not exist.
    $data = [
      'id' => 'contact_formmmm',
      'values' => '{"salutation":"m","first_name":"Roman","email":"roman.domke@unic.com","privacy_policy":true}',
    ];
    $result = $this->executeDataProducer('submit_form', [
      'data' => $data,
      'language' => 'DE',
    ]);

    $this->assertEquals('Webform does not exist.', $result['errors'][0]['message']);

    // Values have the wrong format.
    $data = [
      'id' => 'contact_form',
      'values' => '"{\"salutation\":\"m\",name\":\"Roman\",\"email\":\"roman.domke@unic.com\",\"privacy_policy\":true}"}',
    ];
    $result = $this->executeDataProducer('submit_form', [
      'data' => $data,
      'language' => 'DE',
    ]);

    $this->assertEquals('Submitted values are not well formed.', $result['errors'][0]['message']);

    // Validation errors from drupal.
    $data = [
      'id' => 'contact_form',
      'values' => '{"salutation":"m","email":"roman.domke","privacy_policy":true}',
    ];
    $result = $this->executeDataProducer('submit_form', [
      'data' => $data,
      'language' => 'DE',
    ]);

    $this->assertEquals('#first_name', $result['errors'][0]['element'], 'first_name');
    $this->assertEquals('First name field is required.', $result['errors'][0]['message']);
    $this->assertEquals('#email', $result['errors'][1]['element']);
    $this->assertEquals('The email address <em class="placeholder">roman.domke</em> is not valid. Use the format user@example.com.', $result['errors'][1]['message']);
  }

}
