# Example webform query in a router query.

```
{
  router(path: "/node/8") {
    ... on ErrorResponse {
      code
      message
    }
    ... on RedirectResponse {
      code
      target
    }
    ... on EntityResponse {
      entity {
        nid
        title
        content_elements {
          ... on Layout {
            regions {
              ... on Region {
                paragraphs {
                  ...Form
                }
              }
            }
          }
        }
      }
    }
  }
}

fragment Form on WebformItem {
  published
  title
  webform {
    webform_id
    elements {
      __typename
      ... on WebformSelect {
        name
        title
        type
        required
        rules
        placeholder
        description
        options {
          key
          value
          id
          description
        }
      }
      ... on WebformCheckbox {
        name
        title
        type
        required
        rules
        placeholder
        description
        options {
          key
          value
          id
          description
        }
      }
      ... on WebformCheckboxes {
        name
        title
        type
        required
        rules
        placeholder
        description
        options {
          key
          value
          id
          description
        }
      }
      ... on WebformTextfield {
        name
        title
        type
        required
        rules
        placeholder
        description
        minlength
        maxlength
        size
      }
      ... on WebformEmail {
        name
        title
        type
        required
        rules
        placeholder
        description
        minlength
        maxlength
        size
      }
      ... on WebformTextarea {
        name
        title
        type
        required
        rules
        placeholder
        description
        minlength
        maxlength
        size
        rows
      }
      ... on WebformDate {
        name
        type
        title
        required
        rules
        placeholder
        description
      }
      ... on WebformDatetime {
        name
        type
        title
        required
        rules
        placeholder
        description
      }
    }
  }
}
```

# Example mutation to submit the basic contact form.
```
mutation {
  submitWebform(data: {id: "contact", values: "{\"name\":\"Unic\",\"email\":\"test@unic.com\",\"subject\":\"Lorem\",\"message\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit.\"}"}, language: EN) {
	submission {
      submission_id
      confirmation_message
    }
    errors {
      element
      message
    }
  }
}
```
