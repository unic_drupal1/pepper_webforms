<?php

namespace Drupal\pepper_webforms\Plugin\GraphQL\DataProducer\Webform\Mutation;

use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\RenderContext;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\graphql\GraphQL\Execution\FieldContext;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Drupal\webform\Entity\WebformSubmission;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\webform\Entity\Webform;
use Drupal\webform\WebformSubmissionForm;

/**
 * Creates a new webform submission.
 *
 * @DataProducer(
 *   id = "submit_form",
 *   name = @Translation("Submit webform"),
 *   description = @Translation("Create a new webform submission."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("WebformSubmission")
 *   ),
 *   consumes = {
 *     "data" = @ContextDefinition("any",
 *       label = @Translation("WebformSubmission data")
 *     ),
 *    "language" = @ContextDefinition("string",
 *       label = @Translation("Language"),
 *       required = TRUE
 *     ),
 *   }
 * )
 */
class SubmitForm extends DataProducerPluginBase implements ContainerFactoryPluginInterface {

  /**
   * Drupal language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * @var Webform
   */
  protected $webform;

  /**
   * @var WebformSubmission
   */
  protected $webformSubmission;

  /**
   * @var string
   */
  protected $data;

  /**
   * @var string
   */
  protected $language;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('language_manager'),
      $container->get('renderer')
    );
  }

  /**
   * CreateArticle constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   Drupal language manager.
   */
  public function __construct(array $configuration, string $plugin_id, array $plugin_definition, LanguageManagerInterface $language_manager,  RendererInterface $renderer) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->languageManager = $language_manager;
    $this->renderer = $renderer;
  }

  /**
   * Creates a webform submission.
   *
   * @param array $data
   *   The id of the webform and the values.
   * @param string $language
   *   The language of this request.
   * @param \Drupal\graphql\GraphQL\Execution\FieldContext $context
   *   The field context.
   *
   * @return array
   *   The newly created webform submission or errors.
   *
   * @throws \Exception
   */
  public function resolve(array $data, $language, FieldContext $context) {

    $this->data = $data;
    $this->language = $language;

    // The language parameter is optional, if not set - use default.
    if (empty($language)) {
      $language = $this->languageManager->getDefaultLanguage()->getId();
    }
    else {
      $context->setContextLanguage($language);
      $context->setContextValue('language', $language);
      $this->languageManager->reset();
    }

    // Load the webform in the specified language.
    // See https://drupal.stackexchange.com/a/263807
    $original_language = $this->languageManager->getConfigOverrideLanguage();
    $this->languageManager->setConfigOverrideLanguage($this->languageManager->getLanguage($language));

    $this->webform = Webform::load($data['id']);

    $context = new RenderContext();
    $data = $this->renderer->executeInRenderContext($context, function () {
      if (!$this->webform instanceof Webform) {
        return [
          'submission' => NULL,
          'errors' => [
            [
              'element' => NULL,
              'message' => 'Webform does not exist.',
            ],
          ],
        ];
      }

      // Check if webform is open.
      if (!$this->webform->isOpen()) {
        $closedMessage = $this->webform->getSetting('form_close_message', 'This form is closed.');
        return [
          'submission' => NULL,
          'errors' => [
            [
              'element' => NULL,
              'message' => 'Webform is not open for submissions: ' . $closedMessage,
            ],
          ],
        ];
      }

      // Check if value string is correct.
      $values = $this->prepareValues($this->data, $this->language);
      if (!$values) {
        return [
          'submission' => NULL,
          'errors' => [
            [
              'element' => NULL,
              'message' => 'Submitted values are not well formed.',
            ],
          ],
        ];
      }

      // Create a webform submission.
      /** @var \Drupal\webform\WebformSubmissionInterface $this->webformSubmission */
      $this->webformSubmission = WebformSubmission::create($values);

      // Validate the given values.
      if ($errors = $this->validateValues($values)) {
        return [
          'submission' => NULL,
          'errors' => $errors,
        ];
      }

      // Submit this prepared submission.
      /** @var \Drupal\webform\Entity\WebformSubmission $finalSubmission */
      $finalSubmission = WebformSubmissionForm::submitWebformSubmission($this->webformSubmission);

      $return = [
        'submission' => [
          'submission_id' => $finalSubmission->id(),
          'confirmation_message' => $this->webform->getSetting('confirmation_message', ''),
          'confirmation_title' => $this->webform->getSetting('confirmation_title', ''),
        ],
      ];
      return $return;
    });

    // Reset language back to the defaults.
    $this->languageManager->setConfigOverrideLanguage($original_language);

    return $data;
  }

  /**
   * Prepares the given values and attach them to create a submission.
   *
   * @param array $data
   *   The WebformInput given by the request mutation.
   * @param string $language
   *   The languageId ENUM.
   *
   * @return array|null
   *   The prepared submission values or NULL, if not well formed.
   */
  private function prepareValues(array $data, string $language) {
    // Check if the value string is well formed.
    $values = json_decode($data['values'], TRUE);
    if (!is_array($values)) {
      return NULL;
    }

    // Build values array.
    $valuesArray = [
      'webform_id' => $data['id'],
      'langcode' => $language,
      'data' => $values,
    ];
    return $valuesArray;
  }

  /**
   * Validates the given values against the webform.
   *
   * @param array $values
   *   The given values.
   *
   * @return array
   *   List of errors.
   */
  private function validateValues(array $values) {
    $errors = WebformSubmissionForm::validateFormValues($values);
    if (!$errors) {
      return NULL;
    }
    $errorMessages = [];
    foreach ($errors as $name => $error) {
      $errorMessages[] = [
        'element' => '#' . $name,
        'message' => $error instanceof TranslatableMarkup ? $error->render() : $this->t($error),
      ];
    }
    return $errorMessages;
  }

}
