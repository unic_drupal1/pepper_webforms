<?php

namespace Drupal\pepper_webforms\Plugin\GraphQL\DataProducer\Webform\Types\Text\Textarea;

use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;

/**
 * @DataProducer(
 *   id = "pepper_webforms_types_text_textarea_rows",
 *   name = @Translation("Textarea element rows"),
 *   description = @Translation("Textarea element rows."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("Element")
 *   ),
 *   consumes = {
 *     "element" = @ContextDefinition("any",
 *       label = @Translation("Input array"),
 *       required = FALSE
 *     )
 *   }
 * )
 */
class WebformTypesTextTextareaRows extends DataProducerPluginBase {

  /**
   * Resolver function.
   *
   * @param array $element
   *   The element definition.
   *
   * @return mixed
   *   The elements rows.
   */
  public function resolve(array $element) {
    if (isset($element['#rows'])) {
      return $element['#rows'];
    }
    return NULL;
  }

}
