<?php

namespace Drupal\pepper_webforms\Plugin\GraphQL\DataProducer\Webform\Types\Text;

use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;

/**
 * @DataProducer(
 *   id = "pepper_webforms_types_text_minlength",
 *   name = @Translation("Text element minlength"),
 *   description = @Translation("Text element minlength."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("Element")
 *   ),
 *   consumes = {
 *     "element" = @ContextDefinition("any",
 *       label = @Translation("Input array"),
 *       required = FALSE
 *     )
 *   }
 * )
 */
class WebformTypesTextMinlength extends DataProducerPluginBase {

  /**
   * Resolver function.
   *
   * @param array $element
   *   The element definition.
   *
   * @return mixed
   *   The elements minlength.
   */
  public function resolve(array $element) {
    if (isset($element['#minlength'])) {
      return $element['#minlength'];
    }
    return NULL;
  }

}
