<?php

namespace Drupal\pepper_webforms\Plugin\GraphQL\DataProducer\Webform\Types\Select;

use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;

/**
 * @DataProducer(
 *   id = "pepper_webforms_types_select_options",
 *   name = @Translation("Options for webform element type select"),
 *   description = @Translation("Options for webform element type select."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("Element")
 *   ),
 *   consumes = {
 *     "element" = @ContextDefinition("any",
 *       label = @Translation("Input array"),
 *       required = FALSE
 *     )
 *   }
 * )
 */
class WebformTypesSelectOptions extends DataProducerPluginBase {

  /**
   * Resolver function.
   *
   * This resolves all options for select-ish elements.
   *
   * @param array $element
   *   The element definition.
   *
   * @return mixed
   *   The elements options.
   */
  public function resolve(array $element) {
    switch ($element['#type']) {
      case 'checkboxes':
      case 'select':
      case 'radios':
        return $this->resolveSelectOptions($element);

      case 'checkbox':
        return $this->resolveCheckboxOptions($element);
    }
    return '';
  }

  /**
   * Resolver for "normal" select elements.
   *
   * @param array $element
   *   The element definition.
   *
   * @return array
   *   The elements options.
   */
  private function resolveSelectOptions(array $element) {
    $options = [];
    foreach ($element['#options'] as $key => $value) {
      $options[$key] = [];
      $options[$key]['key'] = $key;
      // For reactive frameworks we need a unique key for each option.
      $options[$key]['id'] = $element['#name'] . '_' . $key;
      if (strpos($value, ' -- ')) {
        $values = $this->getOptionsValues($value);
        $options[$key]['value'] = $values[0];
        $options[$key]['description'] = $values[1];
      }
      else {
        $options[$key]['value'] = $value;
      }
    }
    return $options;
  }

  /**
   * Resolver for a single checkbox element.
   *
   * @param array $element
   *   The element definition.
   *
   * @return array
   *   The elements options.
   */
  private function resolveCheckboxOptions(array $element): array {
    $id = $element['#name'] . '_true';
    return [
      'checkbox' => [
        'key' => TRUE,
        'value' => $element['#title'],
        'id' => $id,
      ],
    ];
  }

  /**
   * This function extracts option description from option values.
   *
   * @param string $value
   *   A single option value.
   *
   * @return array
   *   Value and description separated.
   */
  private function getOptionsValues(string $value): array {
    return explode(' -- ', $value);
  }

}