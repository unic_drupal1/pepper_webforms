<?php

namespace Drupal\pepper_webforms\Plugin\GraphQL\DataProducer\Webform\Types\Select;

use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;

/**
 * @DataProducer(
 *   id = "pepper_webforms_types_select_option",
 *   name = @Translation("A single option for webform element type select"),
 *   description = @Translation("A single option for webform element type select."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("Element")
 *   ),
 *   consumes = {
 *     "element" = @ContextDefinition("any",
 *       label = @Translation("Input array"),
 *       required = FALSE
 *     ),
 *     "field" = @ContextDefinition("string",
 *       label = @Translation("Seek field")
 *     )
 *   }
 * )
 */
class WebformTypesSelectOption extends DataProducerPluginBase {

  /**
   * Resolver function.
   *
   * @param array $options
   *   The elements options.
   * @param string $field
   *   The seeked field.
   *
   * @return mixed
   *   The key or value.
   */
  public function resolve(array $options, $field) {
    try {
      if (isset($options[$field])) {
        return $options[$field];
      }
      return '';
    }
    catch (\OutOfBoundsException $e) {
      return NULL;
    }
  }

}