<?php

namespace Drupal\pepper_webforms\Plugin\GraphQL\DataProducer\Webform\Elements;

use Drupal\Core\Render\Element;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;

/**
 * @DataProducer(
 *   id = "pepper_webforms_flexbox_children",
 *   name = @Translation("Webform Flexbox children"),
 *   description = @Translation("Returns the children of a Webform Flexbox element."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("Element")
 *   ),
 *   consumes = {
 *     "element" = @ContextDefinition("any",
 *       label = @Translation("Input array"),
 *       required = FALSE
 *     )
 *   }
 * )
 */
class WebformFlexboxChildren extends DataProducerPluginBase {

  /**
   * Resolver function.
   *
   * @param array $element
   *   The element definition.
   *
   * @return mixed
   *   The element type.
   */
  public function resolve(array $element) {
    $children = Element::children($element);
    $returned = [];
    foreach ($children as $child) {
      $element[$child]['#name'] = $child;
      $returned[] = $element[$child];
    }
    return $returned;
  }

}
