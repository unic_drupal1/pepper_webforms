<?php

namespace Drupal\pepper_webforms\Plugin\GraphQL\DataProducer\Webform\Elements;

use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;

/**
 * @DataProducer(
 *   id = "pepper_webforms_element_requiredMessage",
 *   name = @Translation("Webform element required message"),
 *   description = @Translation("Provides the required message, defined in the WebformElement interface."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("Element")
 *   ),
 *   consumes = {
 *     "element" = @ContextDefinition("any",
 *       label = @Translation("Input array"),
 *       required = FALSE
 *     )
 *   }
 * )
 */
class WebformElementRequiredMessage extends DataProducerPluginBase {

  /**
   * Resolver function.
   *
   * @param array $element
   *   The element definition.
   *
   * @return mixed
   *   The element required.
   */
  public function resolve(array $element) {
    // Test if the field is present, if not - its not required.
    if (isset($element['#required_error'])) {
      return $element['#required_error'];
    }
    return NULL;
  }

}
