<?php

namespace Drupal\pepper_webforms\Plugin\GraphQL\DataProducer\Webform\Elements;

use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;

/**
 * @DataProducer(
 *   id = "pepper_webforms_element_rules",
 *   name = @Translation("Webform element rules"),
 *   description = @Translation("Provides the rules field, defined in the WebformElement interface."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("Element")
 *   ),
 *   consumes = {
 *     "element" = @ContextDefinition("any",
 *       label = @Translation("Input array"),
 *       required = FALSE
 *     )
 *   }
 * )
 */
class WebformElementRules extends DataProducerPluginBase {

  /**
   * Resolver function.
   *
   * @param array $element
   *   The element definition.
   *
   * @return mixed
   *   The element rules.
   */
  public function resolve(array $element) {
    // See https://logaretm.github.io/vee-validate/guide/rules.html#importing-rules-in-nuxt-js
    // for more infos about rules.
    $rules = [];
    if (isset($element['#required'])) {
      $rules[] = 'required';
    }
    if (isset($element['#disabled'])) {
      $rules[] = 'disabled';
    }
    if ($element['#type'] == 'email') {
      $rules[] = 'email';
    }
    if (isset($element['#min'])) {
      $rules[] = 'min:' . $element['#min'];
    }
    if (isset($element['#max'])) {
      $rules[] = 'max:' . $element['#max'];
    }

    # Between min and max length.
    if (isset($element['#minlength']) && isset($element['#maxlength'])) {
      $rules[] = 'length:' . $element['#minlength'] . "," . $element['#maxlength'];
    }
    # Less than max length.
    elseif (isset($element['#maxlength'])) {
      $rules[] = 'length:0,' . $element['#maxlength'];
    }
    # Greater than a min length.
    elseif (isset($element['#minlength'])) {
      $rules[] = 'length:' . $element['#minlength'];
    }

    if (isset($element['#pattern'])) {
      $rules[] = 'pattern:' . $element['#pattern'];
    }
    $rules = implode('|', $rules);
    return $rules;

  }

}
