<?php

namespace Drupal\pepper_webforms\Plugin\GraphQL\DataProducer\Webform\Elements;

use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;

/**
 * @DataProducer(
 *   id = "pepper_webforms_element_title",
 *   name = @Translation("Webform element title"),
 *   description = @Translation("Provides the title field, defined in the WebformElement interface."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("Element")
 *   ),
 *   consumes = {
 *     "element" = @ContextDefinition("any",
 *       label = @Translation("Input array"),
 *       required = FALSE
 *     )
 *   }
 * )
 */
class WebformElementTitle extends DataProducerPluginBase {

  /**
   * Resolver function.
   *
   * @param array $element
   *   The element definition.
   *
   * @return mixed
   *   The element title.
   */
  public function resolve(array $element) {
    try {
      return isset($element['#title']) ? $element['#title'] : '';
    }
    catch (\OutOfBoundsException $e) {
      return NULL;
    }
  }

}
