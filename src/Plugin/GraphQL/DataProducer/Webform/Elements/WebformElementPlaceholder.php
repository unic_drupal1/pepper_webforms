<?php

namespace Drupal\pepper_webforms\Plugin\GraphQL\DataProducer\Webform\Elements;

use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;

/**
 * @DataProducer(
 *   id = "pepper_webforms_element_placeholder",
 *   name = @Translation("Webform element placeholder"),
 *   description = @Translation("Provides the placeholder field, defined in the WebformElement interface."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("Element")
 *   ),
 *   consumes = {
 *     "element" = @ContextDefinition("any",
 *       label = @Translation("Input array"),
 *       required = FALSE
 *     )
 *   }
 * )
 */
class WebformElementPlaceholder extends DataProducerPluginBase {

  /**
   * Resolver function.
   *
   * @param array $element
   *   The element definition.
   *
   * @return mixed
   *   The element name.
   */
  public function resolve(array $element) {
    if (isset($element['#placeholder'])) {
      return $element['#placeholder'];
    }
    return NULL;
  }

}
