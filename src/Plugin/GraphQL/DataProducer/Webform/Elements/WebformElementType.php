<?php

namespace Drupal\pepper_webforms\Plugin\GraphQL\DataProducer\Webform\Elements;

use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;

/**
 * @DataProducer(
 *   id = "pepper_webforms_element_type",
 *   name = @Translation("Webform element type"),
 *   description = @Translation("Provides the type field, defined in the WebformElement interface."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("Element")
 *   ),
 *   consumes = {
 *     "element" = @ContextDefinition("any",
 *       label = @Translation("Input array"),
 *       required = FALSE
 *     )
 *   }
 * )
 */
class WebformElementType extends DataProducerPluginBase {

  /**
   * Resolver function.
   *
   * @param array $element
   *   The element definition.
   *
   * @return mixed
   *   The element type.
   */
  public function resolve(array $element) {
    try {
      return $element['#type'];
    }
    catch (\OutOfBoundsException $e) {
      return NULL;
    }
  }

}
