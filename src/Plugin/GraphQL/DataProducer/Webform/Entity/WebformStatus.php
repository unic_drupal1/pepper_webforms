<?php

namespace Drupal\pepper_webforms\Plugin\GraphQL\DataProducer\Webform\Entity;

use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Drupal\webform\Entity\Webform;

/**
 * @DataProducer(
 *   id = "pepper_webforms_webform_status",
 *   name = @Translation("Webform Status"),
 *   description = @Translation("Provides webform status."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("Element")
 *   ),
 *   consumes = {
 *     "webform" = @ContextDefinition("any",
 *       label = @Translation("Webform"),
 *       required = FALSE
 *     )
 *   }
 * )
 */
class WebformStatus extends DataProducerPluginBase {

  /**
   * Returns the status of a given webform.
   *
   * @param \Drupal\webform\Entity\Webform $webform
   *   The webform.
   *
   * @return string
   *   The webform status.
   */
  public function resolve(Webform $webform) {
    if ($webform->isOpen()) {
      return 'open';
    }
    elseif ($webform->isScheduled()) {
      return 'scheduled';
    }
    else {
      return 'closed';
    }
  }

}
