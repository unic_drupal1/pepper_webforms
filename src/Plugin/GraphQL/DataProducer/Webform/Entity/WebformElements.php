<?php

namespace Drupal\pepper_webforms\Plugin\GraphQL\DataProducer\Webform\Entity;

use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\graphql\GraphQL\Execution\FieldContext;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Drupal\webform\Entity\Webform;
use Drupal\webform\WebformTranslationManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @DataProducer(
 *   id = "pepper_webforms_webform_elements",
 *   name = @Translation("Webform Elements"),
 *   description = @Translation("Provides webform elements."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("Element")
 *   ),
 *   consumes = {
 *     "webform" = @ContextDefinition("any",
 *       label = @Translation("Webform"),
 *       required = FALSE
 *     )
 *   }
 * )
 */
class WebformElements extends DataProducerPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The webform translation manager service.
   *
   * @var \Drupal\webform\WebformTranslationManagerInterface
   */
  protected $translationManager;

  /**
   * {@inheritdoc}
   *
   * @codeCoverageIgnore
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('webform.translation_manager')
    );
  }

  /**
   * EntityLoad constructor.
   *
   * @param array $configuration
   *   The plugin configuration array.
   * @param string $pluginId
   *   The plugin id.
   * @param array $pluginDefinition
   *   The plugin definition array.
   * @param \Drupal\webform\WebformTranslationManagerInterface $translationManager
   *   The entity type manager service.
   *
   * @codeCoverageIgnore
   */
  public function __construct(
    array $configuration,
    string $pluginId,
    array $pluginDefinition,
    WebformTranslationManagerInterface $translationManager
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
    $this->translationManager = $translationManager;
  }

  /**
   * Returns all webform elements.
   *
   * @param \Drupal\webform\Entity\Webform $webform
   *   The webform.
   * @param \Drupal\graphql\GraphQL\Execution\FieldContext $context
   *   The caching context related to the current field.
   *
   * @return array
   *   The webform elements.
   */
  public function resolve(Webform $webform, FieldContext $context) {

    $language = $context->getContextLanguage();
    if ($language instanceof LanguageInterface) {
      $language = $language->getId();
    }

    // Take care of translated elements.
    $config_translation = \Drupal::moduleHandler()->moduleExists('config_translation');

    if ($config_translation) {
      $original = $this->translationManager->getElements($webform);
      $translation = $this->translationManager->getElements($webform, $language);
      $elements = array_replace_recursive($original, $translation);
    }
    else {
      $elements = $webform->getElementsDecodedAndFlattened();
    }

    // Check if any element has no type. This can happen when a form is cloned
    // with a leftover of a former translation.
    foreach ($elements as $key => $element) {
      if (!isset($element['#type'])) {
        unset($elements[$key]);
      }
    }

    // Attach the name.
    foreach ($elements as $name => &$element) {
      $element['#name'] = $name;
    }

    // Filter out the submit label, because it is no real element.
    if (isset($elements['actions'])) {
      unset($elements['actions']);
    }

    return $elements;
  }

}
