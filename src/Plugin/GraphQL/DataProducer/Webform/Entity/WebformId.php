<?php

namespace Drupal\pepper_webforms\Plugin\GraphQL\DataProducer\Webform\Entity;

use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Drupal\webform\Entity\Webform;

/**
 * @DataProducer(
 *   id = "pepper_webforms_webform_id",
 *   name = @Translation("Webform Id"),
 *   description = @Translation("Provides webform id."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("Element")
 *   ),
 *   consumes = {
 *     "webform" = @ContextDefinition("any",
 *       label = @Translation("Webform"),
 *       required = FALSE
 *     )
 *   }
 * )
 */
class WebformId extends DataProducerPluginBase {

  /**
   * Returns an id to a given webform.
   *
   * @param \Drupal\webform\Entity\Webform $webform
   *   The webform.
   *
   * @return int
   *   The webform id.
   */
  public function resolve(Webform $webform) {
    return $webform->id();
  }

}
