<?php

namespace Drupal\pepper_webforms\Plugin\GraphQL\DataProducer\Webform\Message;

use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\graphql\GraphQL\Execution\FieldContext;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Drupal\webform\Entity\Webform;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @DataProducer(
 *   id = "pepper_webforms_webform_close_message",
 *   name = @Translation("Webform closed message"),
 *   description = @Translation("Provides webform closed message."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("Element")
 *   ),
 *   consumes = {
 *     "webform" = @ContextDefinition("any",
 *       label = @Translation("Webform"),
 *       required = FALSE
 *     )
 *   }
 * )
 */
class WebformFormClosedMessage extends DataProducerPluginBase implements ContainerFactoryPluginInterface {

  /**
   * Drupal language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('language_manager'),
    );
  }

  /**
   * CreateArticle constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   Drupal language manager.
   */
  public function __construct(array $configuration, string $plugin_id, array $plugin_definition, LanguageManagerInterface $language_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->languageManager = $language_manager;
  }

  /**
   * Returns a string with the closed message.
   *
   * @param \Drupal\webform\Entity\Webform $webform
   *   The webform.
   *
   * @return string
   *   The closed message.
   */
  public function resolve(Webform $webform, FieldContext $context) {
    $original_language = $this->languageManager->getConfigOverrideLanguage();
    $this->languageManager->setConfigOverrideLanguage($this->languageManager->getLanguage($context->getContextLanguage()));

    $this->webform = Webform::load($webform->id());

    // Reset language back to the defaults.
    $this->languageManager->setConfigOverrideLanguage($original_language);

    return $this->webform->getSetting('form_close_message');
  }

}
