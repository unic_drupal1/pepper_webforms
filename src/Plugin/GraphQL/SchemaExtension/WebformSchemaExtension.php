<?php

namespace Drupal\pepper_webforms\Plugin\GraphQL\SchemaExtension;

use Drupal\graphql\GraphQL\ResolverBuilder;
use Drupal\graphql\GraphQL\ResolverRegistryInterface;
use Drupal\graphql\Plugin\GraphQL\SchemaExtension\SdlSchemaExtensionPluginBase;
use GraphQL\Error\Error;

/**
 * @SchemaExtension(
 *   id = "pepper_webform",
 *   name = "Webform schema extension",
 *   description = "Adds webform elements and mutations.",
 *   schema = "custom_composable"
 * )
 */
class WebformSchemaExtension extends SdlSchemaExtensionPluginBase {

  /**
   * The type how it is known to the schema.
   *
   * @var array
   */
  protected $elementTypes = [
    'WebformSelect',
    'WebformTextfield',
    'WebformEmail',
    'WebformCheckbox',
    'WebformCheckboxes',
    'WebformDatetime',
    'WebformTextarea',
    'WebformDate',
    'WebformRadios',
    'WebformUrl',
    'WebformNumber',
    'WebformFlexbox',
    'WebformMarkup',
  ];

  /**
   * All needed fields, defined in the WebformElement interface.
   *
   * @var array
   */
  protected $baseFields = [
    'name',
    'type',
    'title',
    'required',
    'disabled',
    'rules',
    'placeholder',
    'description',
    'requiredMessage',
  ];

  /**
   * Maps the schema languageId ENUM to Drupal language ids.
   *
   * Note! This is not the mapping to the path prefix itself. Here we only map
   * the incoming ENUM from GraphQL to the language id of Drupal.
   *
   * @var array
   */
  private $languageIdMapping = [
    'EN' => 'en',
    'DE' => 'de',
    'FR' => 'fr',
    'IT' => 'it',
  ];

  /**
   * {@inheritdoc}
   */
  public function registerResolvers(ResolverRegistryInterface $registry) {
    $builder = new ResolverBuilder();

    $this->addTypeFields($registry, $builder);
  }

  /**
   * Adds the content fields that are needed to the query.
   *
   * @param \Drupal\graphql\GraphQL\ResolverRegistryInterface $registry
   *   The registry of the resolvers.
   * @param \Drupal\graphql\GraphQL\ResolverBuilder $builder
   *   The builder of the resolvers.
   */
  protected function addTypeFields(ResolverRegistryInterface $registry, ResolverBuilder $builder) {
    $registry->addTypeResolver('WebformElement', function ($value) {
      if (isset($value['#type'])) {
        switch ($value['#type']) {
          case 'webform_flexbox':
            return 'WebformFlexbox';
          case 'webform_markup':
            return 'WebformMarkup';

          default:
            return 'Webform' . ucfirst($value['#type']);
        }
      }
      throw new Error('Could not resolve WebformElement type of type ' . $value['#type'] . '.');
    });

    $registry->addFieldResolver('WebformItem', 'paragraph_id',
      $builder->produce('entity_id')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('WebformItem', '_id',
      $builder->produce('entity_uuid')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('WebformItem', 'published',
      $builder->produce('entity_published')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('WebformItem', 'title',
      $builder->produce('property_path')
        ->map('type', $builder->fromValue('entity:paragraph'))
        ->map('value', $builder->fromParent())
        ->map('path', $builder->fromValue('field_title.value'))
    );

    $registry->addFieldResolver('WebformItem', 'webform',
      $builder->compose(
        $builder->produce('entity_reference')
          ->map('entity', $builder->fromParent())
          ->map('field', $builder->fromValue('field_form'))
          ->map('access', $builder->fromValue(FALSE)),
        $builder->produce('seek')
          ->map('input', $builder->fromParent())
          ->map('position', $builder->fromValue(0))
      )
    );

    $registry->addFieldResolver('Webform', 'webform_id',
      $builder->produce('pepper_webforms_webform_id')
        ->map('webform', $builder->fromParent())
    );

    $registry->addFieldResolver('Webform', 'elements',
      $builder->produce('pepper_webforms_webform_elements')
        ->map('webform', $builder->fromParent())
    );

    // Add submit button label.
    $registry->addFieldResolver('Webform', 'submit_label',
      $builder->produce('pepper_webforms_types_submit_button_label')
        ->map('webform', $builder->fromParent())
    );

    // Add webform status.
    $registry->addFieldResolver('Webform', 'status',
      $builder->produce('pepper_webforms_webform_status')
        ->map('webform', $builder->fromParent())
    );

    $registry->addFieldResolver('Webform', 'form_closed_message',
      $builder->produce('pepper_webforms_webform_close_message')
        ->map('webform', $builder->fromParent())
    );

    // Add all needed fields, defined in the WebformElement interface.
    foreach ($this->elementTypes as $elementType) {
      foreach ($this->baseFields as $baseField) {
        $registry->addFieldResolver($elementType, $baseField,
          $builder->produce('pepper_webforms_element_' . $baseField)
            ->map('element', $builder->fromParent())
        );
      }
    }

    // Add select element specific fields.
    $registry->addFieldResolver('WebformSelect', 'options',
      $builder->produce('pepper_webforms_types_select_options')
        ->map('element', $builder->fromParent())
    );

    $registry->addFieldResolver('WebformSelectOption', 'id',
      $builder->produce('pepper_webforms_types_select_option')
        ->map('element', $builder->fromParent())
        ->map('field', $builder->fromValue('id'))
    );

    $registry->addFieldResolver('WebformSelectOption', 'key',
      $builder->produce('pepper_webforms_types_select_option')
        ->map('element', $builder->fromParent())
        ->map('field', $builder->fromValue('key'))
    );

    $registry->addFieldResolver('WebformSelectOption', 'value',
      $builder->produce('pepper_webforms_types_select_option')
        ->map('element', $builder->fromParent())
        ->map('field', $builder->fromValue('value'))
    );

    $registry->addFieldResolver('WebformSelectOption', 'description',
      $builder->produce('pepper_webforms_types_select_option')
        ->map('element', $builder->fromParent())
        ->map('field', $builder->fromValue('description'))
    );

    $registry->addFieldResolver('WebformCheckbox', 'options',
      $builder->produce('pepper_webforms_types_select_options')
        ->map('element', $builder->fromParent())
    );

    $registry->addFieldResolver('WebformCheckboxes', 'options',
      $builder->produce('pepper_webforms_types_select_options')
        ->map('element', $builder->fromParent())
    );

    $registry->addFieldResolver('WebformRadios', 'options',
      $builder->produce('pepper_webforms_types_select_options')
        ->map('element', $builder->fromParent())
    );

    // Add text element specific fields.
    foreach (['WebformTextfield', 'WebformTextarea'] as $type) {
      foreach (['maxlength', 'minlength', 'size'] as $field) {
        $registry->addFieldResolver($type, $field,
          $builder->produce('pepper_webforms_types_text_' . $field)
            ->map('element', $builder->fromParent())
        );
      }
    }

    $registry->addFieldResolver('WebformTextarea', 'rows',
      $builder->produce('pepper_webforms_types_text_textarea_rows')
        ->map('element', $builder->fromParent())
    );

    // Add markup specific field.
    $registry->addFieldResolver('WebformMarkup', 'markup',
      $builder->produce('pepper_webforms_types_markup_markup')
        ->map('element', $builder->fromParent())
    );

    // Add mutation to submit a webform.
    $registry->addFieldResolver('Mutation', 'submitWebform',
      $builder->produce('submit_form')
        ->map('data', $builder->fromArgument('data'))
        ->map('language', $builder->callback(function ($value, $args) {

          // Allow empty language parameter, as its optional.
          if (empty($args['language'])) {
            return '';
          }

          if (isset($this->languageIdMapping[$args['language']])) {
            return $this->languageIdMapping[$args['language']];
          }

          throw new Error('Could not resolve language.');
        }))
    );


    $registry->addFieldResolver('WebformFlexbox', 'children',
      $builder->produce('pepper_webforms_flexbox_children')
        ->map('element', $builder->fromParent())
    );

  }

}
